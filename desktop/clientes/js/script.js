$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {  
            return;
        }
        if (isElementInViewport($(value))) {
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            $(value).addClass('start');
        }
    });
}
$(window).scroll(function() {
    checkAnimation();
});


// Adiciona a class de animação no html
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animated'),
        animationClass = 'fadeInDownBig',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 250);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});

//animacão dos botões 
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animBot'),
        animationClass = 'bounceIn',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 80);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});

//Animação dos text
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animText'),
        animationClass = 'fadeInRight',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 190);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});

//Animação dos text
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animText2'),
        animationClass = 'fadeInLeft',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 190);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});


// var animateElementRight = function(e) {
//     var clas = document.getElementById('ani')
//     jQuery(clas).each(function(i) {
//         var top_of_object = jQuery(this).offset().top;
//         var bottom_of_window = jQuery(window).scrollTop() + jQuery(window).height();
//         if ((bottom_of_window) > top_of_object) {
//             $.each($(".start"), function() {
//                 jQuery(this).addClass('animText fadeInRight');
//             });
//         }
//     });
// };
// var animateElementLeft = function(e) {
//     jQuery("#ani2").each(function(i) {
//         var top_of_object = jQuery(this).offset().top;
//         var bottom_of_window = jQuery(window).scrollTop() + jQuery(window).height();
//         if ((bottom_of_window) > top_of_object) {
//             return;
//         } else {
//             $.each($(".start"), function() {
//                 jQuery(this).addClass('animText fadeInLeft');
//             });
//         }
//     });
// };

//Inicia a animação

// Troca as imagens quando chega no Topo
// function scrollFunction() {
//     if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
//         document.getElementById("logo").style.display = "block";
//     } else {
//         document.getElementById("logo").style.display = "none";
//     }
//     if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
//         document.getElementById("contact").style.display = "none"
//         document.getElementById("contact1").style.display = "block"
//     } else {
//         document.getElementById("contact").style.display = "block";
//         document.getElementById("contact1").style.display = "none"
//     }
// }

//Capture scroll events




// // Get a reference to the <path>
// var path = document.querySelector('#cs1');

// // Get length of path... ~577px in this case
// var pathLength = path.getTotalLength();

// // Make very long dashes (the length of the path itself)
// path.style.strokeDasharray = pathLength + ' ' + pathLength;

// // Offset the dashes so the it appears hidden entirely
// path.style.strokeDashoffset = pathLength;

// // Jake Archibald says so
// // https://jakearchibald.com/2013/animated-line-drawing-svg/
// path.getBoundingClientRect();

// // When the page scrolls...
// window.addEventListener("scroll", function(e) {

//     // What % down is it? 
//     // https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript/2387222#2387222
//     // Had to try three or four differnet methods here. Kind of a cross-browser nightmare.
//     var scrollPercentage = (document.documentElement.scrollTop + document.body.scrollTop) / (document.documentElement.scrollHeight - document.documentElement.clientHeight);

//     // Length to offset the dashes
//     var drawLength = pathLength * scrollPercentage;

//     // Draw in reverse
//     path.style.strokeDashoffset = pathLength - drawLength;

//     // When complete, remove the dash array, otherwise shape isn't quite sharp
//     // Accounts for fuzzy math
//     if (scrollPercentage >= 0.99) {
//         path.style.strokeDasharray = "none";

//     } else {
//         path.style.strokeDasharray = pathLength + ' ' + pathLength;
//     }

// });


jQuery('#btn1').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('#btn2').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('#btn3').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})

$(document).ready(function() {
    var hamb = $('#hamb');
    var $target = $('#n-menu');
    var close = $('#x');
    $(hamb).on('click', function() {
        $($target).addClass('active');
        document.getElementById('hamb').style.display = 'none'
    })
    $(close).on('click', function() {
        $($target).removeClass('active')
        document.getElementById('hamb').style.display = 'block'
    })
})