$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}


// Adiciona a class de animação no html
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animated'),
        animationClass = 'fadeInDownBig',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 240);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});
// abrir auto o botão
$(function() {
    var $target = $('#num1'),
        nsClass = 'active',
        windowHe = $(window).height(),
        offs = windowHe - (windowHe - 100);

    function sanScroll() {
        var docTop = $(document).scrollTop();
        $target.each(function() {
            if (docTop > boxTop(this) - offs) {
                return;
            } else {
                setTimeout(function() {
                    $($target).addClass(nsClass);
                }, 700);
            }
        })
    }
    sanScroll();
    $(document).ready(function() {
        setTimeout(function() { sanScroll() }, 1)
    })
})


// Inicia a animação
window.onscroll = function() {
    scrollFunction();

};
// Troca as imagens quando chega no Topo
function scrollFunction() {
    if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
        document.getElementById("logo").style.display = "none";
    } else {
        document.getElementById("logo").style.display = "none";
    }
    if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
        document.getElementById("contact").style.display = "none"
        document.getElementById("contact1").style.display = "none"
    } else {
        document.getElementById("contact").style.display = "block";
        document.getElementById("contact1").style.display = "none"
    }
}

// Capture scroll events
$(window).scroll(function() {
    checkAnimation();
});

$(function() {
    $('#num1').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    $('#num2').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    $('#num3').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    $('#num4').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    $('#num5').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    $('#num6').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    $('#num7').on('click', function() {
        $('.box').removeClass('active');
        $(this).addClass(' active');
    })
    jQuery('#box1').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#box2').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
})

jQuery('#prep').on('click', function() {
    var an = $('#bol1').hasClass('animated');
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
        $(an).addClass('animated');
    } else {
        $(this).addClass('active');
        $(an).removeClass('animated');
    }
})

jQuery('#loc').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})

$(document).ready(function() {
    var hamb = $('#hamb');
    var $target = $('#n-menu');
    var close = $('#x');
    $(hamb).on('click', function() {
        $($target).addClass('active');
        document.getElementById('hamb').style.display = 'none'
    })
    $(close).on('click', function() {
        $($target).removeClass('active')
        document.getElementById('hamb').style.display = 'block'
    })
})