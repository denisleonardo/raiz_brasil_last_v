$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');

    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top + 100);
    var elemBottom = elemTop + $elem.height();
    console.log(elemTop)

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            // return console.log(s);
            return 0;
        }
        if (isElementInViewport($(value))) {
            console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            i.classList.add('start');
        }
    });
}
$(window).scroll(function() {
    checkAnimation();
});

// Inicia a animação
window.onscroll = function() {
    scrollFunction();

};
// Troca as imagens quando chega no Topo
function scrollFunction() {
    if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
        document.getElementById("logo").style.display = "none";
    } else {
        document.getElementById("logo").style.display = "none";
    }
    if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
        document.getElementById("contact").style.display = "none"
        document.getElementById("contact1").style.display = "none"
    } else {
        document.getElementById("contact").style.display = "block";
        document.getElementById("contact1").style.display = "none"
    }
}

// Adiciona a class de animação no html
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animated'),
        animationClass = 'fadeInDownBig',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 250);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});

jQuery('#prep').on('click', function() {
    var an = $('#bol1').hasClass('animated');
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
        $(an).addClass('animated');
    } else {
        $(this).addClass('active');
        $(an).removeClass('animated');
    }
})
jQuery('#loc').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('#sol').on('click', function() {
    var i = document.getElementById('loc');
    var l = document.getElementById('prep');

    if (jQuery(i, l).hasClass('active')) {
        $(i).removeClass('active');
        $(l).removeClass('active');
    } else {
        $(i).addClass('active');
        $(l).addClass('active');
    }
})
$(function() {
    $('#circulo-1').on('click', function() {
        // ac = $('#num1');
        $('.circles').removeClass('active');
        $(this).addClass(' active');
    })
    $('#circulo-2').on('click', function() {
        $('.circles').removeClass('active');
        $(this).addClass(' active');
    })
    $('#circulo-3').on('click', function() {
        $('.circles').removeClass('active');
        $(this).addClass(' active');
    })
    $('#circulo-4').on('click', function() {
        $('.circles').removeClass('active');
        $(this).addClass(' active');
    })
    $('#circulo-5').on('click', function() {
        $('.circles').removeClass('active');
        $(this).addClass(' active');
    })
    $('#circulo-6').on('click', function() {
        $('.circles').removeClass('active');
        $(this).addClass(' active');
    })
})


// Get a reference to the <path>
// var path = document.querySelector('#arco1');
// // Get length of path... ~577px in this demo
// var pathLength = path.getTotalLength();
// console.log(pathLength);

// // Make very long dashes (the length of the path itself)
// path.style.strokeDasharray = pathLength + ' ' + pathLength;
// // Offset the dashes so the it appears hidden entirely
// path.style.strokeDashoffset = pathLength;

// // When the page scrolls...
// window.addEventListener("scroll", function(e) {
//     // What % down is it? 
//     var scrollPercentage = (document.documentElement.scrollTop + document.body.scrollTop) / (document.documentElement.scrollHeight - document.documentElement.clientHeight);
//     // Length to offset the dashes
//     var drawLength = pathLength * scrollPercentage;
//     // Draw in reverse
//     path.style.strokeDashoffset = pathLength - drawLength;
//   });

// new Vivus('SVGitem', {
//     duration: 200,
//     forceRender: /^((?!chrome|android).)*(msie|edge|trident|safari)/i.test(window.navigator.userAgent)
// });

$(document).ready(function() {
    var hamb = $('#hamb');
    var $target = $('#n-menu');
    var close = $('#x');
    $(hamb).on('click', function() {
        $($target).addClass('active');
        document.getElementById('hamb').style.display = 'none'
    })
    $(close).on('click', function() {
        $($target).removeClass('active')
        document.getElementById('hamb').style.display = 'block'
    })
})