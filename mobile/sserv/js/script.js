(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            $(value).addClass('start');
        }
    });
}
// Capture scroll events
$(window).scroll(function() {
    checkAnimation();
});


$(function() {
    jQuery('#circle-1').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#circle-2').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#circle-3').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#circle-4').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#dem-1').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#dem-2').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#dem-3').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#dem-4').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#dem-5').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    jQuery('#dem-6').on('click', function() {
        if (jQuery(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
})
$(document).ready(function() {
    var hamb = $('#hamb');
    var $target = $('#n-menu');
    var close = $('#x');
    $(hamb).on('click', function() {
        $($target).addClass('active');
        document.getElementById('hamb').style.display = 'none'
    })
    $(close).on('click', function() {
        $($target).removeClass('active')
        document.getElementById('hamb').style.display = 'block'
    })
})

$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}