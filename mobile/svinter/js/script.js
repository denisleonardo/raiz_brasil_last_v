(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}
// Capture scroll events
$(window).scroll(function() {
    checkAnimation();
});

// $(function() {
//     $('.circle-1').on('click', function() {
//         $('.box').removeClass('active');
//         $(this).addClass(' active');
//     })
//     $('.circle-2').on('click', function() {
//         $('.box').removeClass('active');
//         $(this).addClass(' active');
//     })
//     $('.circle-3').on('click', function() {
//         $('.box').removeClass('active');
//         $(this).addClass(' active');
//     })
//     $('.circle-4').on('click', function() {
//         $('.box').removeClass('active');
//         $(this).addClass(' active');
//     })
//     $('.circle-5').on('click', function() {
//         $('.box').removeClass('active');
//         $(this).addClass(' active');
//     })
//     $('.circle-6').on('click', function() {
//         $('.box').removeClass('active');
//         $(this).addClass(' active');
//     })
// })

jQuery('.circle-1').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('.circle-2').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('.circle-3').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('.circle-4').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('.circle-5').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
jQuery('.circle-6').on('click', function() {
    if (jQuery(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})
//animacão dos botões 
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animBot'),
        animationClass = 'bounceIn',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 80);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});


// Adiciona a class de animação no html
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animated'),
        animationClass = 'fadeInDownBig',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 250);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});
$(function() {
    var $target = $('#prep');
    var $target_2 = $('#loc');
    $($target).on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    $($target_2).on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
})

$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}
$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}

$(document).ready(function() {
    var hamb = $('#hamb');
    var $target = $('#n-menu');
    var close = $('#x');
    $(hamb).on('click', function() {
        $($target).addClass('active');
        document.getElementById('hamb').style.display = 'none'
    })
    $(close).on('click', function() {
        $($target).removeClass('active')
        document.getElementById('hamb').style.display = 'block'
    })
})