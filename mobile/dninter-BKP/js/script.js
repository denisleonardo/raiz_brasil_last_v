$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}


// Adiciona a class de animação no html
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animated'),
        animationClass = 'fadeInDown',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight + 130);

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop > boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});

// // abrir auto o botão
// $(document).ready(function() {
//     var $target = $('#num1'),
//         nClass = 'active',
//         windowHeight = $(window).height(),
//         offset = windowHeight - (windowHeight + 100);

//     function anScroll() {
//         var docTop = $(document).scrollTop();
//         $target.each(function() {
//             if (docTop > boxTop(this) - offset) {
//                 return;
//             } else {
//                 setTimeout(function() {
//                     $($target).addClass(nClass);
//                 }, 700)
//             }
//         })
//     }
//     anScroll();
//     $(document).scroll(function() {
//         setTimeout(function() { anScroll() }, 1);
//     })
// })

// // abrir auto o botão
// $(document).ready(function() {
//     var $target = $('.p-seta'),
//         nsClass = 'op-1',
//         windowHe = $(window).height(),
//         offs = windowHe - (windowHe + 10);

//     function sanScroll() {
//         var docTop = $(document).scrollTop();
//         $target.each(function() {
//             if (docTop > boxTop(this) - offs) {
//                 return;
//             } else {
//                 setTimeout(function() {
//                     $($target).addClass(nsClass);
//                 }, 700)
//             }
//         })
//     }
//     sanScroll();
//     $(document).scroll(function() {
//         setTimeout(function() { sanScroll() }, 1);
//     })
// })


// // Inicia a animação
// window.onscroll = function() {
//     scrollFunction();

// };
// // Troca as imagens quando chega no Topo
// function scrollFunction() {
//     if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
//         document.getElementById("logo").style.display = "block";
//     } else {
//         document.getElementById("logo").style.display = "none";
//     }
//     if (document.body.scrollTop > 30 || document.documentElement.scrollTop > 30) {
//         document.getElementById("contact").style.display = "none"
//         document.getElementById("contact1").style.display = "block"
//     } else {
//         document.getElementById("contact").style.display = "block";
//         document.getElementById("contact1").style.display = "none"
//     }
// }

// Capture scroll events
$(window).scroll(function() {
    checkAnimation();
});

// var objDiv = document.getElementById("inicio");
// objDiv.scrollTop = objDiv.scrollHeight;
$("#inicio").scrollTop($("#inicio")[0].scrollHeight);
$(function() {
    $('#num1').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    $('#num2').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    $('#num3').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    $('#num4').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    $('#num5').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
    $('#num6').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
})

//mask bg-blakc e blur
$(function() {
    var $target = $('#barC1');
    var con = $('#content');
    var mas = $('#mask');

    $($target).on('click', function() {
        if ($(mas).hasClass('start')) {
            $(mas).removeClass('start');
            $(con).removeClass('blur');
        } else {
            $(mas).addClass('start');
            $(con).addClass('blur');

        }
    })
    $($target).on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
})
$(function() {
    var $target = $('#barC2');
    var con = $('#content');
    var mas = $('#mask');

    $($target).on('click', function() {
        if ($(mas).hasClass('start')) {
            $(mas).removeClass('start');
            $(con).removeClass('blur');
        } else {
            $(mas).addClass('start');
            $(con).addClass('blur');

        }
    })
    $($target).on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    })
})

$(function() {
    // scroll all the way down
    $('html, body').scrollTop($(document).height() - ($(window).height()));
});

function isElementInViewport(elem) {
    var $elem = $(elem);
    // Get the scroll position of the page.
    var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'html' : 'html');
    var viewportTop = $(scrollElem).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    // Get the position of the element on the page.
    var elemTop = Math.round($elem.offset().top);
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
    var $elem = $('.ann');
    // var $elem = $('.ann');
    $.each($elem, function(index, value) {
        // console.log($(value).attr('id'))
        if ($(value).hasClass('start')) {
            return;
        }
        if (isElementInViewport($(value))) {
            //console.log($(value).attr('id'));
            id_obj = $(value).attr('id')
            i = document.querySelector('#' + id_obj);
            // console.log(i)
            $(value).addClass('start');
            // i.classList.add('start');
        }
    });
}
$(document).ready(function() {
    var hamb = $('#hamb');
    var $target = $('#n-menu');
    var close = $('#x23');
    $(hamb).on('click', function() {
        $($target).addClass('active');
        document.getElementById('hamb').style.display = 'none';
    })
    $(close).on('click', function() {
        $($target).removeClass('active')
        document.getElementById('hamb').style.display = 'block';
    })
})